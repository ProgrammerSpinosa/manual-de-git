COMANDOS PARA TERMINAL

cd (nome da pasta) = entra na pasta
cd .. = volta para o local anterior
cd pasta/pasta1/pasta2 = entra na pasta 2, que está dentro da pasta1, que está dentro da pasta
clear = limpa tela do terminal

mkdir (nome) = cria pasta com esse nome
touch (nome) = cria arquivo com esse nome

CTRL + H = mostra arquivos ocultos
======================================================================================================================

COMANDOS DO GIT

----------------------------------------------------------------------------------------------------------------------

git init = cria repositório .git no local atual

-----------------------------------------------------------------------------------------------------------------------

git status = dá o status atual do repositório (tem que estar na pasta do repositório).
Isso mostra quais arquivos foram adicionados, quais estão, em espera, etc...

-----------------------------------------------------------------------------------------------------------------------

git config --global user.name "nome" = Cria o nome do usuário do git
git config --global user.email "email" = cria o email do usuário do git

-----------------------------------------------------------------------------------------------------------------------

git add (nome do arquivo) = adicionar arquivo na aŕea de espera
git add *(extensão) = adicionar todo os arquivos daquela extensão (.txt por exemplo) na área de espera
git add . = adiciona todos os arquivos da pasta na área de espera

-----------------------------------------------------------------------------------------------------------------------

git commit -m "mensagem" = salva arquivo no repositório com a "mensagem"
git commit -a -m "mensagem" = salva os arquivos pulando o passo do git add
git commit --amend -m "mensagem" = edita os dados enviados naquele commit/edita a mensagem do commit.

-----------------------------------------------------------------------------------------------------------------------

.gitignore = cria arquivo para o git ignorar arquivos.
Você preenche ele com os arquivos que não quer que o git liste. É um arquivo por linha.

-----------------------------------------------------------------------------------------------------------------------

git diff = mostra diferenças nos arquivos da pasta local
git diff --staged = mostra diferenças nos arquivos da área de espera
git log = mostra o histórico de mudanças nos arquivos salvos no repositório
git log -p = coloca os commits em ordem cronológia (mais novo para o mais antigo) e mostra o diff de cada commit
git log -p -n = pega o log dos n commits, do mais novo para o mais antigo, mostrando o diff de cada commit
git log --pretty=online --> Mostra somente a chave de cada commit, junto com as mensagens

-----------------------------------------------------------------------------------------------------------------------

gitk = abre a interface gráfica do git para ver as mudanças nas versões dos arquivos do repositório do Git.

-----------------------------------------------------------------------------------------------------------------------

git checkout -- (nome do arquivo) = descarta modificações feitas no arquivo na pasta de trabalho.
Ele volta para o estado do último commit.

git checkout (tag) = troca os arquivos para os arquivos da tag.

git checkout (nome do branch) = troca para o branch inserido.

-----------------------------------------------------------------------------------------------------------------------

git rm (nome do arquivo) = remove ele do repositório local.
git rm --cached (nome do arquivo) = remove ele da área de espera.

-----------------------------------------------------------------------------------------------------------------------

git tag = lista todas as tags no sistema
git tag -a (nome da tag) -m "(mensagem)" = cria tag no commit atual
git tag -a (nome da tag) (chave do commit) -m "(mensagem)" = cria tag naquele commit
git show (tag) = mostra mais detalhes daquela tag.
git tag -d (nome da tag) = deleta a tag.
-----------------------------------------------------------------------------------------------------------------------

git branch (nome do branch) = cria branch novo.
git branch -d (nome do branch) = apaga branch.
git branch = lista todos os branches existentes
git checkout -b (nome do branch) = cria branch e já entra nele direto.
-----------------------------------------------------------------------------------------------------------------------

git merge (nome do branch) = traz modificações do branch inserido para o branch atual
-----------------------------------------------------------------------------------------------------------------------
git clone (endereço do projeto) = Clona o projeto para a sua máquina
git push (local pra mandar) (local de onde você tá mandando) = manda arquivos

git remote = mostra o nome do servidor do git.

git fetch (nome do servidor) (nome do branch) = copia os arquivos para o branch.
Isso é bom para evitar o merge automático no master.

git push -u (local pra mandar) (local de onde você tá mandando) = deixa os locais como padrão
git pull (local pra mandar) (local de onde você tá mandando) =  pega arquivos do repositório
-----------------------------------------------------------------------------------------------------------------------

